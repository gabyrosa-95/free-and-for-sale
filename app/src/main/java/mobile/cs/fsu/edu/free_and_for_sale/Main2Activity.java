package mobile.cs.fsu.edu.free_and_for_sale;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Config;
import com.firebase.client.Firebase;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class Main2Activity extends AppCompatActivity {
    public static final String FIREBASE_URL="https://free-6f529.firebaseio.com/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
//----------------------get content from login----------------------//
        String login= "error";
        String name = "name error";
        String post=" ";
        String imageName ="";
        String imageUri ="";
        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            login = extras.getString("login");
            name = extras.getString("name");
            post = extras.getString("post");
            imageName= extras.getString("imageName");
            imageUri= extras.getString("imageUri");
            //The key argument here must match that used in the other activity
        } //if it's a string you stored.
//-------------------enter user to db-------------------------//
        Firebase.setAndroidContext(this);
        Firebase ref = new Firebase(FIREBASE_URL);
        Person person = new Person();

        //Adding values
        person.setName(name);
        person.setLogin(login);
        person.setPost(post);
        //Firebase newRef = ref.child("Person").push();
        //newRef.setValue(person);
        ref.child("Person").setValue(person);
        Toast.makeText(getApplicationContext(), imageName , Toast.LENGTH_LONG).show();
        Toast.makeText(getApplicationContext(), name , Toast.LENGTH_LONG).show();
//------------------add image to database-----------------//

        ImageView imgView=(ImageView)findViewById(R.id.imageView);
        imgView.setImageURI(Uri.parse(imageUri));

   /*    FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl("gs://free-6f529.appspot.com");
        //StorageReference imagesRef = storageRef.child("images");
       // StorageReference imageref = storageRef.child(imageName);
        //StorageReference fullimageref = storageRef.child(imageUri);
        Uri file = Uri.fromFile(new File(imageUri));
        StorageReference riversRef = storageRef.child("images/"+file.getLastPathSegment());
      UploadTask  uploadTask = riversRef.putFile(file);

// Register observers to listen for when the download is done or if it fails
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
            }
        });*/
//----------------------retrieve from database---------------//

        final TextView text= (TextView) findViewById(R.id.textView3);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    //Getting the data from snapshot
                    Person person = postSnapshot.getValue(Person.class);

                    //Adding it to a string
                    String string = "Name: "+person.getName()+"\nAddress: "+person.getLogin()+"\n Post:" +person.getPost();

                    //Displaying it on textview
                    text.setText(string);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                System.out.println("The read failed: " + firebaseError.getMessage());
            }
        });
   //-------new post




        Button button1 = (Button)findViewById(R.id.button2);
        button1.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent myIntent = new Intent(Main2Activity.this,
                        MainActivity.class);
                startActivity(myIntent);
            }
        });
    }
}
