package mobile.cs.fsu.edu.free_and_for_sale;

/**
 * Created by gabrielarosa on 12/11/16.
 */
public class Person {
    //name and address string
    private String name;
    private String login;
    private String post;

    public Person() {
      /*Blank default constructor essential for Firebase*/
    }
    //Getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
    public void setPost(String post) {
        this.post = post;
    }
    public String getPost(){
        return post;
    }
}
