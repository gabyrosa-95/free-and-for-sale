package mobile.cs.fsu.edu.free_and_for_sale;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

public class MainActivity extends AppCompatActivity {
    TextView uri ;
    String imageName;
    String imageUri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText mEdit   = (EditText)findViewById(R.id.editText);
        final EditText mEdit1   = (EditText)findViewById(R.id.editText2);
        final EditText mEdit2   = (EditText)findViewById(R.id.editText3);
        boolean flag=false;
       // final TextView uri   = (TextView)findViewById(R.id.textimage);
       // final String login =mEdit.getText().toString();
      //-------load image button
        Button button2 = (Button)findViewById(R.id.button3);

        button2.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 0);
            }});
//------------------------------/
        Button button1 = (Button)findViewById(R.id.button);
        button1.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {


                final String login =mEdit.getText().toString();
                final String name =mEdit1.getText().toString();
                final String post =mEdit2.getText().toString();
                Intent myIntent = new Intent(MainActivity.this,
                        Main2Activity.class);

                myIntent.putExtra("login", login ); //Optional parameters
                myIntent.putExtra("name", name  );
                myIntent.putExtra("post", post );
                myIntent.putExtra("imageName", imageName );
                myIntent.putExtra("imageUri", imageUri );
                String emailparse="";
                boolean loopflag= false;
             //   startActivity(myIntent);
               for(int i=0; i<login.length();i++)
               {
                   if (login.charAt(i)=='@')
                   {
                       loopflag=true;
                       /*while(i<login.length())
                       {
                           Log.i("something",emailparse );
                           emailparse= emailparse +login.charAt(i);
                           i++;
                       }*/
                   }
                    if(loopflag==true)
                    {
                        emailparse= emailparse +login.charAt(i);
                    }

               }
                if(emailparse.equals("@my.fsu.edu")) {
                    startActivity(myIntent);
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "must be an Fsu email" , Toast.LENGTH_LONG).show();
                }
        }
        });
    }
    //----------------activity for click
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        TextView uri   = (TextView)findViewById(R.id.textView4);
        if (resultCode == RESULT_OK) {
            Uri targetUri = data.getData();
            File f = new File(targetUri.toString());

             imageName = f.getName();
            imageUri=targetUri.toString();
            uri.setText(imageName);
          //  uri.setText(targetUri.toString());
        }
    }//------

}
